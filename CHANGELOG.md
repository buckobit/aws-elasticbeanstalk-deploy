# Changelog
During the Alpha period, minor version releases in the 0.x.y range may introduce breaking changes to the task's interface. 

## [0.1.2] - 2018-11-22  - Alpha Release
### Changed
Use quotes for all tasks examples in README.md.

## [0.1.1] - 2018-11-14  - Alpha Release
### Changed
Restructure README.md to match user flow.

## [0.1.0] - 2018-10-17  - Alpha Release
### Added
Initial release of Bitbucket Pipelines AWS Elastic Beanstalk deployment task.
